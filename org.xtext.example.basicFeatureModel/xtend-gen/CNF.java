import com.google.common.base.Objects;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.xtext.example.mydsl.myDsl.Binary;
import org.xtext.example.mydsl.myDsl.Formula;
import org.xtext.example.mydsl.myDsl.Neg;
import org.xtext.example.mydsl.myDsl.Node;
import org.xtext.example.mydsl.myDsl.Root;
import org.xtext.example.mydsl.myDsl.UniNode;

@SuppressWarnings("all")
public class CNF {
  private final List<clause> clauseList = new ArrayList<clause>();
  
  private String name = "";
  
  public CNF(final String str) {
    this.name = str;
  }
  
  public CNF() {
  }
  
  public String getName() {
    return this.name;
  }
  
  public boolean add(final clause c) {
    return this.clauseList.add(c);
  }
  
  public void addList(final List<clause> list) {
    for (final clause c : list) {
      this.clauseList.add(c);
    }
  }
  
  public int getSize() {
    return ((Object[])Conversions.unwrapArray(this.clauseList, Object.class)).length;
  }
  
  @Override
  public String toString() {
    String _xblockexpression = null;
    {
      String str = (this.name + ": ");
      for (final clause c : this.clauseList) {
        String _string = c.toString();
        String _plus = (str + _string);
        String _plus_1 = (_plus + " & ");
        str = _plus_1;
      }
      int _length = str.length();
      int _minus = (_length - 2);
      _xblockexpression = str = str.substring(0, this.max(0, _minus));
    }
    return _xblockexpression;
  }
  
  public List<clause> getList() {
    return this.clauseList;
  }
  
  public int max(final int i, final int j) {
    int _xifexpression = (int) 0;
    if ((i >= j)) {
      _xifexpression = i;
    } else {
      _xifexpression = j;
    }
    return _xifexpression;
  }
  
  public CNF generateCNF(final Root root) {
    CNF _xblockexpression = null;
    {
      String _name = root.getName();
      final CNF cnf = new CNF(_name);
      String _name_1 = root.getName();
      literal _literal = new literal(_name_1, Boolean.valueOf(true));
      clause _clause = new clause(_literal);
      cnf.add(_clause);
      EList<Node> _sons = root.getSons();
      for (final Node son : _sons) {
        {
          cnf.addList(this.generateClause(son));
          if ((son instanceof UniNode)) {
            String _name_2 = root.getName();
            literal _literal_1 = new literal(_name_2, Boolean.valueOf(true));
            final clause c1 = new clause(_literal_1);
            String _name_3 = ((UniNode)son).getName();
            literal _literal_2 = new literal(_name_3, Boolean.valueOf(false));
            c1.add(_literal_2);
            cnf.add(c1);
            String _name_4 = root.getName();
            literal _literal_3 = new literal(_name_4, Boolean.valueOf(false));
            final clause c2 = new clause(_literal_3);
            String _name_5 = ((UniNode)son).getName();
            literal _literal_4 = new literal(_name_5, Boolean.valueOf(true));
            c2.add(_literal_4);
            String _type = ((UniNode)son).getType();
            if (_type != null) {
              switch (_type) {
                case "Mandatory":
                  cnf.add(c2);
                  break;
              }
            }
          } else {
            String _type_1 = son.getType();
            if (_type_1 != null) {
              switch (_type_1) {
                case "OrGroup":
                  cnf.add(this.OrClause(root.getName(), son));
                  break;
                case "XorGroup":
                  cnf.addList(this.XorClause(root.getName(), son));
                  break;
              }
            }
          }
        }
      }
      cnf.addList(this.generateConstraints(root.getConstraint()));
      _xblockexpression = cnf;
    }
    return _xblockexpression;
  }
  
  public List<clause> generateConstraints(final List<Formula> l) {
    ArrayList<clause> _xblockexpression = null;
    {
      ArrayList<clause> list = new ArrayList<clause>();
      for (final Formula f : l) {
        list.add(this.generateFormula(f));
      }
      _xblockexpression = list;
    }
    return _xblockexpression;
  }
  
  public clause generateFormula(final Formula f) {
    clause _xblockexpression = null;
    {
      clause c = new clause();
      if ((f instanceof Neg)) {
        String _feature = ((Neg)f).getF().getFeature();
        literal _literal = new literal(_feature, Boolean.valueOf(false));
        c.add(_literal);
      } else {
        if ((f instanceof Binary)) {
          final clause cl = this.generateFormula(((Binary)f).getLf());
          final clause cr = this.generateFormula(((Binary)f).getRf());
          c.addClause(cl);
          c.addClause(cr);
        } else {
          String _feature_1 = f.getFeature();
          literal _literal_1 = new literal(_feature_1, Boolean.valueOf(true));
          c.add(_literal_1);
        }
      }
      _xblockexpression = c;
    }
    return _xblockexpression;
  }
  
  public clause OrClause(final String GrandFatherName, final Node node) {
    clause _xblockexpression = null;
    {
      literal _literal = new literal(GrandFatherName, Boolean.valueOf(false));
      final clause c = new clause(_literal);
      EList<Node> _sons = node.getSons();
      for (final Node son : _sons) {
        if ((son instanceof UniNode)) {
          String _name = ((UniNode)son).getName();
          literal _literal_1 = new literal(_name, Boolean.valueOf(true));
          c.add(_literal_1);
        }
      }
      _xblockexpression = c;
    }
    return _xblockexpression;
  }
  
  public List<clause> XorClause(final String GrandFatherName, final Node node) {
    ArrayList<clause> _xblockexpression = null;
    {
      ArrayList<clause> list = new ArrayList<clause>();
      literal _literal = new literal(GrandFatherName, Boolean.valueOf(false));
      final clause c = new clause(_literal);
      EList<Node> _sons = node.getSons();
      for (final Node son : _sons) {
        if ((son instanceof UniNode)) {
          String _name = ((UniNode)son).getName();
          literal _literal_1 = new literal(_name, Boolean.valueOf(true));
          c.add(_literal_1);
          EList<Node> _sons_1 = node.getSons();
          for (final Node son2 : _sons_1) {
            if ((son2 instanceof UniNode)) {
              String _name_1 = ((UniNode)son2).getName();
              String _name_2 = ((UniNode)son).getName();
              boolean _notEquals = (!Objects.equal(_name_1, _name_2));
              if (_notEquals) {
                literal _literal_2 = new literal(GrandFatherName, Boolean.valueOf(false));
                clause c2 = new clause(_literal_2);
                String _name_3 = ((UniNode)son).getName();
                literal _literal_3 = new literal(_name_3, Boolean.valueOf(false));
                c2.add(_literal_3);
                String _name_4 = ((UniNode)son2).getName();
                literal _literal_4 = new literal(_name_4, Boolean.valueOf(false));
                c2.add(_literal_4);
                list.add(c2);
              }
            }
          }
        }
      }
      list.add(c);
      _xblockexpression = list;
    }
    return _xblockexpression;
  }
  
  public List<clause> generateClause(final Node node) {
    ArrayList<clause> _xblockexpression = null;
    {
      ArrayList<clause> list = new ArrayList<clause>();
      if ((node instanceof UniNode)) {
        EList<Node> _sons = ((UniNode)node).getSons();
        for (final Node son : _sons) {
          {
            list.addAll(this.generateClause(son));
            if ((son instanceof UniNode)) {
              InputOutput.<String>println(((UniNode)node).getType());
              String _name = ((UniNode)node).getName();
              literal _literal = new literal(_name, Boolean.valueOf(true));
              final clause c1 = new clause(_literal);
              String _name_1 = ((UniNode)son).getName();
              literal _literal_1 = new literal(_name_1, Boolean.valueOf(false));
              c1.add(_literal_1);
              list.add(c1);
              String _name_2 = ((UniNode)node).getName();
              literal _literal_2 = new literal(_name_2, Boolean.valueOf(false));
              final clause c2 = new clause(_literal_2);
              String _name_3 = ((UniNode)son).getName();
              literal _literal_3 = new literal(_name_3, Boolean.valueOf(true));
              c2.add(_literal_3);
              String _type = ((UniNode)son).getType();
              if (_type != null) {
                switch (_type) {
                  case "Mandatory":
                    list.add(c2);
                    break;
                }
              }
            } else {
              String _type_1 = son.getType();
              if (_type_1 != null) {
                switch (_type_1) {
                  case "OrGroup":
                    list.add(this.OrClause(((UniNode)node).getName(), son));
                    break;
                  case "XorGroup":
                    list.addAll(this.XorClause(((UniNode)node).getName(), son));
                    break;
                }
              }
            }
          }
        }
      } else {
        EList<Node> _sons_1 = node.getSons();
        for (final Node son_1 : _sons_1) {
          list.addAll(this.generateClause(son_1));
        }
      }
      _xblockexpression = list;
    }
    return _xblockexpression;
  }
}
