import com.google.common.base.Objects;
import java.util.ArrayList;
import java.util.Random;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.xtext.example.mydsl.myDsl.Binary;
import org.xtext.example.mydsl.myDsl.Formula;
import org.xtext.example.mydsl.myDsl.MultiNode;
import org.xtext.example.mydsl.myDsl.MyDslFactory;
import org.xtext.example.mydsl.myDsl.Neg;
import org.xtext.example.mydsl.myDsl.Node;
import org.xtext.example.mydsl.myDsl.Root;
import org.xtext.example.mydsl.myDsl.UniNode;
import org.xtext.example.mydsl.myDsl.impl.MyDslFactoryImpl;

@SuppressWarnings("all")
public class RandomModelGenerator {
  private Random randGen = new Random();
  
  private final MyDslFactory fact = MyDslFactoryImpl.init();
  
  public Node generateRandomNewSon(final String name, final String fatherType) {
    boolean _equals = Objects.equal(fatherType, "OrGroup");
    if (_equals) {
      final UniNode n = this.fact.createUniNode();
      n.setName(name);
      n.setType("Or");
      return n;
    }
    boolean _equals_1 = Objects.equal(fatherType, "XorGroup");
    if (_equals_1) {
      final UniNode n_1 = this.fact.createUniNode();
      n_1.setName(name);
      n_1.setType("Xor");
      return n_1;
    }
    final int type = this.randGen.nextInt(4);
    if ((type <= 1)) {
      UniNode n_2 = this.fact.createUniNode();
      n_2.setName(name);
      switch (type) {
        case 0:
          n_2.setType("Mandatory");
          break;
        default:
          n_2.setType("Optional");
          break;
      }
      return n_2;
    } else {
      MultiNode n_3 = this.fact.createMultiNode();
      switch (type) {
        case 2:
          n_3.setType("OrGroup");
          break;
        default:
          n_3.setType("XorGroup");
          break;
      }
      return n_3;
    }
  }
  
  public void addSons(final Node n, final int nb, final String subname) {
    final EList<Node> sons = n.getSons();
    for (int i = 0; (i < nb); i++) {
      String _string = Integer.toString(i);
      String _plus = (("f" + subname) + _string);
      sons.add(this.generateRandomNewSon(_plus, n.getType()));
    }
  }
  
  public void setSons(final Node n, final int maxDepth, final int range) {
    if ((maxDepth != 0)) {
      int _nextInt = this.randGen.nextInt(range);
      final int nbSons = (_nextInt + 1);
      this.addSons(n, nbSons, Integer.toString(maxDepth));
      final EList<Node> sons = n.getSons();
      for (final Node son : sons) {
        if ((Objects.equal(son.getType(), "XorGroup") || Objects.equal(son.getType(), "OrGroup"))) {
          this.setSons(son, maxDepth, range);
        } else {
          this.setSons(son, (maxDepth - 1), range);
        }
      }
    }
  }
  
  public ArrayList<String> getSonsNames(final Node n) {
    final ArrayList<String> names = CollectionLiterals.<String>newArrayList();
    final EList<Node> sons = n.getSons();
    for (final Node node : sons) {
      {
        if ((node instanceof UniNode)) {
          names.add(((UniNode)node).getName());
        }
        names.addAll(this.getSonsNames(node));
      }
    }
    return names;
  }
  
  public ArrayList<String> getAllSonsNames(final Root r) {
    final ArrayList<String> names = CollectionLiterals.<String>newArrayList();
    final EList<Node> sons = r.getSons();
    for (final Node node : sons) {
      {
        if ((node instanceof UniNode)) {
          names.add(((UniNode)node).getName());
        }
        names.addAll(this.getSonsNames(node));
      }
    }
    return names;
  }
  
  public Formula getRandomFeature(final String[] names) {
    final int r = this.randGen.nextInt(names.length);
    final Formula f = this.fact.createFormula();
    f.setFeature(names[r]);
    final int pos = this.randGen.nextInt(2);
    if ((pos == 0)) {
      final Neg g = this.fact.createNeg();
      g.setF(f);
      return g;
    }
    return f;
  }
  
  public Binary Or(final Formula f1, final Formula f2) {
    final Binary formula = this.fact.createBinary();
    formula.setLf(f1);
    formula.setOp("or");
    formula.setRf(f2);
    return formula;
  }
  
  public Binary And(final Formula f1, final Formula f2) {
    final Binary formula = this.fact.createBinary();
    formula.setLf(f1);
    formula.setOp("and");
    formula.setRf(f2);
    return formula;
  }
  
  public Formula getRandomClause(final String[] names, final int nbLiteral) {
    Formula f = this.getRandomFeature(names);
    for (int i = 0; (i < nbLiteral); i++) {
      f = this.Or(f, this.getRandomFeature(names));
    }
    return f;
  }
  
  public Root newModel(final int depth, final int range, final int nbliteral, final int nbClause) {
    final Root root = this.fact.createRoot();
    root.setName("Root");
    int _nextInt = this.randGen.nextInt((range - 1));
    final int nbSons = (_nextInt + 1);
    for (int i = 0; (i < nbSons); i++) {
      String _string = Integer.toString(depth);
      String _plus = ("f" + _string);
      String _string_1 = Integer.toString(i);
      String _plus_1 = (_plus + _string_1);
      root.getSons().add(this.generateRandomNewSon(_plus_1, "Root"));
    }
    final EList<Node> sons = root.getSons();
    for (final Node son : sons) {
      this.setSons(son, (depth - 1), range);
    }
    final EList<Formula> constraints = root.getConstraint();
    final ArrayList<String> names = this.getAllSonsNames(root);
    for (int i = 0; (i < nbClause); i++) {
      constraints.add(this.getRandomClause(((String[])Conversions.unwrapArray(names, String.class)), (nbliteral - 1)));
    }
    return root;
  }
  
  public static void main(final String[] args) {
    final RandomModelGenerator Test = new RandomModelGenerator();
    final writeToFile Write = new writeToFile();
    final Root truc = Test.newModel(2, 3, 2, 1);
    InputOutput.<String>println(Write.prettyPrint(truc));
  }
}
