import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

@SuppressWarnings("all")
public class testWizard {
  public static void main(final String[] args) {
    Display display = Display.getCurrent();
    if ((display == null)) {
      Display _display = new Display();
      display = _display;
    }
    myWizard wizard = new myWizard();
    Shell shell = new Shell(display);
    WizardDialog dialog = new WizardDialog(shell, wizard);
    dialog.open();
  }
}
