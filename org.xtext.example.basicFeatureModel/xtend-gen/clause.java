import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
public class clause {
  private final List<literal> clause = new ArrayList<literal>();
  
  public clause() {
  }
  
  public clause(final literal l) {
    this.add(l);
  }
  
  public List<literal> get() {
    return this.clause;
  }
  
  public boolean add(final literal l) {
    return this.clause.add(l);
  }
  
  public boolean addClause(final clause c) {
    return this.clause.addAll(c.get());
  }
  
  @Override
  public String toString() {
    String _xblockexpression = null;
    {
      String str = "(";
      for (final literal l : this.clause) {
        String _string = l.toString();
        String _plus = (str + _string);
        String _plus_1 = (_plus + "|");
        str = _plus_1;
      }
      int _length = str.length();
      int _minus = (_length - 1);
      String _substring = str.substring(0, this.max(0, _minus));
      String _plus_2 = (_substring + ")");
      _xblockexpression = str = _plus_2;
    }
    return _xblockexpression;
  }
  
  public int max(final int i, final int j) {
    int _xifexpression = (int) 0;
    if ((i >= j)) {
      _xifexpression = i;
    } else {
      _xifexpression = j;
    }
    return _xifexpression;
  }
}
