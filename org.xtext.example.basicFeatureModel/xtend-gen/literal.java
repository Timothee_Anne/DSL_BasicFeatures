@SuppressWarnings("all")
public class literal {
  private Boolean bool = Boolean.valueOf(true);
  
  private String name = "";
  
  public literal(final String str, final Boolean b) {
    this.bool = b;
    this.name = str;
  }
  
  public String get() {
    return this.name;
  }
  
  public Boolean toBool() {
    return this.bool;
  }
  
  @Override
  public String toString() {
    String _xifexpression = null;
    if ((this.bool).booleanValue()) {
      _xifexpression = this.name;
    } else {
      _xifexpression = ("~" + this.name);
    }
    return _xifexpression;
  }
}
