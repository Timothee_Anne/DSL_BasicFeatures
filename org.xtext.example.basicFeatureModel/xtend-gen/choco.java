import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.nary.cnf.LogOp;
import org.chocosolver.solver.variables.BoolVar;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.xtext.example.mydsl.myDsl.Root;

@SuppressWarnings("all")
public class choco {
  private static Hashtable<String, BoolVar> table = new Hashtable<String, BoolVar>();
  
  private static Model model = new Model("model");
  
  private static BoolVar tmp = choco.model.boolVar("tmp");
  
  public static LogOp cnfToChocoCnf(final CNF cnf) {
    BoolVar f = choco.model.boolVar(false);
    LogOp clauses = LogOp.nor(f);
    List<clause> _list = cnf.getList();
    for (final clause c : _list) {
      clauses = LogOp.and(clauses, choco.clauseToLogOp(c));
    }
    return clauses;
  }
  
  public static LogOp clauseToLogOp(final clause cl) {
    BoolVar t = choco.model.boolVar(true);
    LogOp clause = LogOp.nor(t);
    List<literal> _get = cl.get();
    for (final literal l : _get) {
      {
        boolean _containsKey = choco.table.containsKey(l.get());
        boolean _not = (!_containsKey);
        if (_not) {
          choco.tmp = choco.model.boolVar(l.get());
          choco.table.put(l.get(), choco.tmp);
        } else {
          choco.tmp = choco.table.get(l.get());
        }
        Boolean _bool = l.toBool();
        if ((_bool).booleanValue()) {
          clause = LogOp.or(clause, choco.tmp);
        } else {
          clause = LogOp.or(clause, LogOp.nor(choco.tmp));
        }
      }
    }
    return clause;
  }
  
  public static void main(final String[] args) {
    final RandomModelGenerator mGen = new RandomModelGenerator();
    final CNF cnfGen = new CNF();
    final writeToFile write = new writeToFile();
    final Root test = write.rootExample();
    final CNF testcnf = cnfGen.generateCNF(test);
    final LogOp testchocoCnf = choco.cnfToChocoCnf(testcnf);
    choco.model.addClauses(testchocoCnf);
    InputOutput.<LogOp>println(testchocoCnf);
    InputOutput.<Model>println(choco.model);
    Collection<BoolVar> _values = choco.table.values();
    for (final BoolVar bv : _values) {
      System.out.println(bv);
    }
    System.out.println("\n");
    final Solver solver = choco.model.getSolver();
    while (solver.solve()) {
      {
        Collection<BoolVar> _values_1 = choco.table.values();
        for (final BoolVar bv_1 : _values_1) {
          System.out.println(bv_1);
        }
        System.out.println("\n");
      }
    }
  }
}
