import com.google.common.base.Charsets;
import com.google.common.io.Files;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.reader.ParseFormatException;
import org.sat4j.reader.Reader;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;
import org.xtext.example.mydsl.myDsl.Binary;
import org.xtext.example.mydsl.myDsl.Formula;
import org.xtext.example.mydsl.myDsl.MultiNode;
import org.xtext.example.mydsl.myDsl.MyDslFactory;
import org.xtext.example.mydsl.myDsl.Neg;
import org.xtext.example.mydsl.myDsl.Node;
import org.xtext.example.mydsl.myDsl.Root;
import org.xtext.example.mydsl.myDsl.UniNode;
import org.xtext.example.mydsl.myDsl.impl.MyDslFactoryImpl;

@SuppressWarnings("all")
public class writeToFile {
  public static void main(final String[] args) {
    try {
      final writeToFile instance = new writeToFile();
      final RandomModelGenerator rand = new RandomModelGenerator();
      final Root root = rand.newModel(2, 3, 2, 1);
      final String code = instance.prettyPrint(root);
      String _name = root.getName();
      String _plus = (_name + ".txt");
      final File file = new File(_plus);
      Files.write(code, file, Charsets.UTF_8);
      String _absolutePath = file.getAbsolutePath();
      String _plus_1 = ("Generated code to " + _absolutePath);
      InputOutput.<String>println(_plus_1);
      final CNF cnf = new CNF();
      final CNF cnfFromRoot = cnf.generateCNF(root);
      InputOutput.<CNF>println(cnfFromRoot);
      String _name_1 = cnfFromRoot.getName();
      String cnfName = (_name_1 + ".cnf");
      final File cnfFile = new File(cnfName);
      Files.write(instance.toDIMACS(cnfFromRoot), cnfFile, Charsets.UTF_8);
      String _name_2 = cnfFromRoot.getName();
      final File z3File = new File(_name_2);
      Files.write(instance.toZ3(cnfFromRoot), z3File, Charsets.UTF_8);
      writeToFile.analyseFileCnf(cnfName);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public static void analyseFileCnf(final String fileName) {
    final ISolver solver = SolverFactory.newDefault();
    solver.setTimeout(3600);
    final Reader reader = new DimacsReader(solver);
    try {
      final IProblem problem = reader.parseInstance(fileName);
      boolean _isSatisfiable = problem.isSatisfiable();
      if (_isSatisfiable) {
        System.out.println("Satisfiable !");
        System.out.println(problem.model());
      } else {
        System.out.println("Unsatisfiable !");
      }
    } catch (final Throwable _t) {
      if (_t instanceof FileNotFoundException) {
        final FileNotFoundException e = (FileNotFoundException)_t;
        System.out.println("File");
      } else if (_t instanceof ParseFormatException) {
        final ParseFormatException e_1 = (ParseFormatException)_t;
        System.out.println(e_1);
      } else if (_t instanceof IOException) {
        final IOException e_2 = (IOException)_t;
        System.out.println("IOE");
      } else if (_t instanceof ContradictionException) {
        final ContradictionException e_3 = (ContradictionException)_t;
        System.out.println("Unsatisfiable (trivial)!");
      } else if (_t instanceof TimeoutException) {
        final TimeoutException e_4 = (TimeoutException)_t;
        System.out.println("Timeout, sorry!");
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  public String prettyPrint(final Root root) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Root: ");
    String _name = root.getName();
    _builder.append(_name);
    _builder.newLineIfNotEmpty();
    _builder.append("Sons:");
    _builder.newLine();
    _builder.append("(");
    _builder.newLine();
    {
      EList<Node> _sons = root.getSons();
      for(final Node son : _sons) {
        String _printNode = this.printNode(son);
        _builder.append(_printNode);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append(")");
    _builder.newLine();
    return _builder.toString();
  }
  
  public String printNode(final Node node) {
    StringConcatenation _builder = new StringConcatenation();
    String _switchResult = null;
    String _type = node.getType();
    if (_type != null) {
      switch (_type) {
        case "Mandatory":
          _switchResult = "M";
          break;
        case "Optional":
          _switchResult = "O";
          break;
        case "Xor":
          _switchResult = "xor";
          break;
        case "Or":
          _switchResult = "or";
          break;
        case "XorGroup":
          _switchResult = "Xor";
          break;
        case "OrGroup":
          _switchResult = "Or";
          break;
        default:
          _switchResult = "Not a good type";
          break;
      }
    } else {
      _switchResult = "Not a good type";
    }
    String _plus = (_switchResult + " ");
    String _printNodeName = this.printNodeName(node);
    String _plus_1 = (_plus + _printNodeName);
    _builder.append(_plus_1);
    _builder.newLineIfNotEmpty();
    _builder.append("Sons :");
    _builder.newLine();
    _builder.append("(");
    _builder.newLine();
    {
      EList<Node> _sons = node.getSons();
      for(final Node son : _sons) {
        String _printNode = this.printNode(son);
        _builder.append(_printNode);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append(")");
    _builder.newLine();
    return _builder.toString();
  }
  
  public String printNodeName(final Node node) {
    StringConcatenation _builder = new StringConcatenation();
    {
      if ((node instanceof UniNode)) {
        String _name = ((UniNode)node).getName();
        _builder.append(_name);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }
  
  public String toDIMACS(final CNF cnf) {
    String _xblockexpression = null;
    {
      final int n = cnf.getSize();
      final Hashtable<String, Integer> H = new Hashtable<String, Integer>();
      int i = 1;
      List<clause> _list = cnf.getList();
      for (final clause c : _list) {
        List<literal> _get = c.get();
        for (final literal l : _get) {
          {
            final String id = l.get();
            boolean _containsKey = H.containsKey(id);
            boolean _not = (!_containsKey);
            if (_not) {
              H.put(id, Integer.valueOf(i));
              i++;
            }
          }
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("p cnf ");
      _builder.append(i);
      _builder.append(" ");
      _builder.append(n);
      _builder.newLineIfNotEmpty();
      {
        List<clause> _list_1 = cnf.getList();
        for(final clause c_1 : _list_1) {
          {
            List<literal> _get_1 = c_1.get();
            for(final literal l_1 : _get_1) {
              String _xifexpression = null;
              Boolean _bool = l_1.toBool();
              boolean _not = (!(_bool).booleanValue());
              if (_not) {
                _xifexpression = "-";
              }
              _builder.append(_xifexpression);
              Integer _get_2 = H.get(l_1.get());
              _builder.append(_get_2);
              _builder.append(" ");
            }
          }
          _builder.append("0");
          _builder.newLineIfNotEmpty();
        }
      }
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  public Root rootExample() {
    final MyDslFactory factory = MyDslFactoryImpl.init();
    final Root root = factory.createRoot();
    root.setName("root");
    final UniNode Mnode = factory.createUniNode();
    Mnode.setName("Mnode");
    Mnode.setType("Mandatory");
    final UniNode Onode2 = factory.createUniNode();
    Onode2.setName("Onode2");
    Onode2.setType("Optional");
    final EList<Node> MnodeSons = Mnode.getSons();
    MnodeSons.add(Onode2);
    final UniNode Onode = factory.createUniNode();
    Onode.setName("Onode");
    Onode.setType("Optional");
    final MultiNode Xnode = factory.createMultiNode();
    Xnode.setType("XorGroup");
    final UniNode xson = factory.createUniNode();
    xson.setName("x42");
    xson.setType("Xor");
    final UniNode xsonson = factory.createUniNode();
    xsonson.setName("s42");
    xsonson.setType("Mandatory");
    final EList<Node> xsonsons = xson.getSons();
    xsonsons.add(xsonson);
    final UniNode xson2 = factory.createUniNode();
    xson2.setName("x73");
    xson2.setType("Xor");
    final EList<Node> sons = Xnode.getSons();
    sons.add(xson);
    sons.add(xson2);
    final MultiNode Ornode = factory.createMultiNode();
    Ornode.setType("OrGroup");
    final UniNode orson = factory.createUniNode();
    orson.setName("or1");
    orson.setType("Or");
    final UniNode orson2 = factory.createUniNode();
    orson2.setName("or2");
    orson2.setType("Or");
    final EList<Node> orsons = Ornode.getSons();
    orsons.add(orson);
    orsons.add(orson2);
    final EList<Node> Rsons = root.getSons();
    Rsons.add(Onode);
    Rsons.add(Mnode);
    Rsons.add(Ornode);
    Rsons.add(Xnode);
    final EList<Formula> constraints = root.getConstraint();
    final Binary F = factory.createBinary();
    final Formula lf = factory.createFormula();
    lf.setFeature("Onode");
    final Binary rf = factory.createBinary();
    final Neg lrf = factory.createNeg();
    final Formula nlrf = factory.createFormula();
    nlrf.setFeature("or1");
    lrf.setF(nlrf);
    final Formula rrf = factory.createFormula();
    rrf.setFeature("x73");
    rf.setLf(lrf);
    rf.setRf(rrf);
    rf.setOp("or");
    F.setLf(lf);
    F.setRf(rf);
    F.setOp("or");
    final Neg G = factory.createNeg();
    final Formula g = factory.createFormula();
    g.setFeature("Onode2");
    G.setF(g);
    constraints.add(F);
    constraints.add(G);
    return root;
  }
  
  public String toZ3(final CNF cnf) {
    String _xblockexpression = null;
    {
      final Hashtable<String, Integer> H = new Hashtable<String, Integer>();
      StringConcatenation _builder = new StringConcatenation();
      {
        List<clause> _list = cnf.getList();
        for(final clause c : _list) {
          {
            List<literal> _get = c.get();
            for(final literal l : _get) {
              String _xifexpression = null;
              boolean _containsKey = H.containsKey(l.get());
              boolean _not = (!_containsKey);
              if (_not) {
                String _xblockexpression_1 = null;
                {
                  H.put(l.get(), Integer.valueOf(1));
                  String _get_1 = l.get();
                  String _plus = ("(declare-const " + _get_1);
                  _xblockexpression_1 = (_plus + " Bool )");
                }
                _xifexpression = _xblockexpression_1;
              }
              _builder.append(_xifexpression);
              _builder.newLineIfNotEmpty();
            }
          }
        }
      }
      _builder.append("(define-fun model () Bool");
      _builder.newLine();
      {
        List<clause> _list_1 = cnf.getList();
        for(final clause c_1 : _list_1) {
          _builder.append("(and ");
          {
            List<literal> _get_1 = c_1.get();
            for(final literal l_1 : _get_1) {
              _builder.append("(or ");
              String _xifexpression_1 = null;
              Boolean _bool = l_1.toBool();
              boolean _not_1 = (!(_bool).booleanValue());
              if (_not_1) {
                String _get_2 = l_1.get();
                String _plus = ("(not " + _get_2);
                _xifexpression_1 = (_plus + ")");
              } else {
                _xifexpression_1 = l_1.get();
              }
              _builder.append(_xifexpression_1);
              _builder.append(" ");
            }
          }
          _builder.append("false ");
          {
            List<literal> _get_3 = c_1.get();
            for(final literal l_2 : _get_3) {
              _builder.append(")");
            }
          }
          _builder.newLineIfNotEmpty();
          _builder.append("\t ");
          _builder.append("true ");
        }
      }
      {
        List<clause> _list_2 = cnf.getList();
        for(final clause c_2 : _list_2) {
          _builder.append(")");
        }
      }
      _builder.append(")");
      _builder.newLineIfNotEmpty();
      _builder.append("(push)");
      _builder.newLine();
      _builder.append("(assert model)");
      _builder.newLine();
      _builder.append("(check-sat)");
      _builder.newLine();
      _builder.append("(pop)");
      _builder.newLine();
      _builder.append("(push)");
      _builder.newLine();
      {
        Set<String> _keySet = H.keySet();
        for(final String k : _keySet) {
          _builder.newLine();
          _builder.append("(assert (not (or ");
          _builder.append(k);
          _builder.append(" (not model) ) ) ) ");
          _builder.newLineIfNotEmpty();
          _builder.append("(check-sat)");
          _builder.newLine();
          _builder.append("(pop)");
          _builder.newLine();
          _builder.append("(push)");
          _builder.newLine();
        }
      }
      _builder.newLine();
      {
        Set<String> _keySet_1 = H.keySet();
        for(final String k_1 : _keySet_1) {
          _builder.newLine();
          _builder.append("(assert (not (or (not ");
          _builder.append(k_1);
          _builder.append(") (not model) ) ) ) ");
          _builder.newLineIfNotEmpty();
          _builder.append("(check-sat)");
          _builder.newLine();
          _builder.append("(pop)");
          _builder.newLine();
          _builder.append("(push)");
          _builder.newLine();
        }
      }
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
}
