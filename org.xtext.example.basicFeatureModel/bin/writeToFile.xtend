
import org.xtext.example.mydsl.myDsl.impl.MyDslFactoryImpl;

import java.io.File
import com.google.common.io.Files
import com.google.common.base.Charsets
import org.xtext.example.mydsl.myDsl.Root
import org.xtext.example.mydsl.myDsl.Node
import org.xtext.example.mydsl.myDsl.UniNode
import java.util.Hashtable
import org.sat4j.minisat.SolverFactory
import org.sat4j.specs.ISolver
import org.sat4j.reader.DimacsReader
import org.sat4j.reader.Reader
import org.sat4j.specs.IProblem
import java.io.FileNotFoundException
import org.sat4j.reader.ParseFormatException
import java.io.IOException
import org.sat4j.specs.ContradictionException
import org.sat4j.specs.TimeoutException


class writeToFile {
	
	
	def static void main(String[] args) {
		
        val instance = new writeToFile
        val rand = new RandomModelGenerator
        
	    val root = rand.newModel(2, 3, 2, 1)
	   	//val root = instance.rootExample()
	   	
	    val code = instance.prettyPrint(root)
	    val file = new File(root.name+".txt")
	    Files.write(code, file, Charsets.UTF_8)
	    println("Generated code to " + file.absolutePath)
	    	       
	    val cnf = new CNF
	    val cnfFromRoot = cnf.generateCNF(root)
	    
	    println(cnfFromRoot)
		var cnfName = cnfFromRoot.getName()+".cnf"
	    val cnfFile = new File(cnfName)
	    Files.write(instance.toDIMACS(cnfFromRoot), cnfFile, Charsets.UTF_8)
	    
	    val z3File = new File(cnfFromRoot.getName())
	    Files.write(instance.toZ3(cnfFromRoot), z3File, Charsets.UTF_8)
	    ///println("Generated code to " + cnfFile.absolutePath)

	    analyseFileCnf(cnfName)
}




def static void analyseFileCnf( String fileName){
	    val ISolver solver = SolverFactory.newDefault();
        solver.setTimeout(3600); // 1 hour timeout
        val Reader reader = new DimacsReader(solver);
        // CNF filename is given on the command line 
        
        try {
            val IProblem problem = reader.parseInstance(fileName);
            if (problem.isSatisfiable()) {
                System.out.println("Satisfiable !");
                System.out.println(problem.model());
            } else {
                System.out.println("Unsatisfiable !");
            }
        } catch (FileNotFoundException e) {
        	System.out.println("File");
        } catch (ParseFormatException e) {
        	System.out.println(e);
        } catch (IOException e) {
        	System.out.println("IOE");
        } catch (ContradictionException e) {
            System.out.println("Unsatisfiable (trivial)!");
        } catch (TimeoutException e) {
            System.out.println("Timeout, sorry!");      
        }
}

def String prettyPrint(Root root) '''
        Root: «root.name»
        Sons:
        (
        «FOR son : root.sons»
        «printNode(son) »
        «ENDFOR »
        )
    '''

def String printNode(Node node) ''' 
	«switch node.type {
		case "Mandatory" : "M"
		case "Optional": "O"
		case "Xor": "xor"
		case "Or": "or"
		case "XorGroup": "Xor"
		case "OrGroup": "Or"
		default: "Not a good type"
	} + " " + printNodeName(node)»
	Sons :
	(
	«FOR son : node.sons»
	«printNode(son) »
	«ENDFOR »
	)
'''
	
def String printNodeName(Node node) ''' 	
	« IF node instanceof UniNode »
	« node.name»
	«ENDIF »
'''

def String toDIMACS(CNF cnf){
	val n = cnf.getSize()
	val H = new Hashtable<String,Integer>()
	var i = 1
    for (clause c: cnf.getList()){
    	for (literal l: c.get()){
    		val id = l.get()
    		if (!H.containsKey(id)){
    			H.put(id,i)
    			i++
    		}
    	}
    }
	'''p cnf «i» «n»
	«FOR  c: cnf.getList() »
«FOR  l:c.get() »«if(!l.toBool())"-"»«H.get(l.get())» «ENDFOR»0
	«ENDFOR»
	'''
}
def Root rootExample(){
		val factory = MyDslFactoryImpl.init()
		val root = factory.createRoot()
		root.setName("root")
		
		
		val Mnode = factory.createUniNode()
		Mnode.setName("Mnode")
		Mnode.setType("Mandatory")
		
		val Onode2 = factory.createUniNode()
		Onode2.setName("Onode2")
		Onode2.setType("Optional")
		
		val MnodeSons = Mnode.getSons()
		MnodeSons.add(Onode2) 
		
		val Onode = factory.createUniNode()
		Onode.setName("Onode")
		Onode.setType("Optional")
		
		val Xnode = factory.createMultiNode()
		Xnode.setType("XorGroup")
		
			val xson = factory.createUniNode()
			xson.setName("x42")
			xson.setType("Xor")
		
				val xsonson = factory.createUniNode()
				xsonson.setName("s42")
				xsonson.setType("Mandatory")
				
				val xsonsons = xson.getSons()
				xsonsons.add(xsonson)
				
			val xson2 = factory.createUniNode()
			xson2.setName("x73")
			xson2.setType("Xor")
			
			val sons = Xnode.getSons()
			sons.add(xson)
			sons.add(xson2)
			
		val Ornode = factory.createMultiNode()
		Ornode.setType("OrGroup")
		
			val orson = factory.createUniNode()
			orson.setName("or1")
			orson.setType("Or")
		
			val orson2 = factory.createUniNode()
			orson2.setName("or2")
			orson2.setType("Or")
			
			val orsons = Ornode.getSons()
			orsons.add(orson)
			orsons.add(orson2)
			
		val Rsons = root.getSons()	
		Rsons.add(Onode )
		Rsons.add(Mnode)
    	Rsons.add(Ornode)
        Rsons.add(Xnode)
        
	    val constraints = root.getConstraint()
	    val F =  factory.createBinary()
	    	val lf = factory.createFormula()
	    	lf.setFeature("Onode")
	    	val rf = factory.createBinary()
	    		val lrf = factory.createNeg()
	    		val nlrf = factory.createFormula()
	    		nlrf.setFeature("or1")
	    		lrf.setF(nlrf)
	    		val rrf = factory.createFormula()
	    		rrf.setFeature("x73")
	    		rf.setLf(lrf)
	    		rf.setRf(rrf)
	    		rf.setOp("or")
	    
	    F.setLf(lf)
	    F.setRf(rf)
	    F.setOp("or")
	    
	    val G = factory.createNeg()
	    	val g = factory.createFormula() 
	    	g.setFeature("Onode2")
	    
	    G.setF(g)
	    
	    constraints.add(F)
	    constraints.add(G)
	    return root
	}
	
def String toZ3(CNF cnf){
	val H = new Hashtable<String,Integer>()
'''«FOR c: cnf.getList()»«FOR l: c.get()»«if(!H.containsKey(l.get())){H.put(l.get(),1);"(declare-const "+ l.get()+" Bool )" }»
«ENDFOR»«ENDFOR»
(define-fun model () Bool
 «FOR  c: cnf.getList() »
(and «FOR  l:c.get() »(or «if(!l.toBool()) {"(not "+l.get()+")"} else {l.get()}» «ENDFOR»false «FOR  l:c.get() »)«ENDFOR»
	 true «ENDFOR»«FOR  c: cnf.getList() »)«ENDFOR»)
(push)
(assert model)
(check-sat)
(pop)
(push)
«FOR k: H.keySet()»

(assert (not (or «k» (not model) ) ) ) 
(check-sat)
(pop)
(push)
«ENDFOR»

«FOR k: H.keySet()»

(assert (not (or (not «k») (not model) ) ) ) 
(check-sat)
(pop)
(push)
«ENDFOR»
'''



}
	
}