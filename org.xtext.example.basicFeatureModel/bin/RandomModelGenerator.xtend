import org.xtext.example.mydsl.myDsl.impl.MyDslFactoryImpl
import java.util.Random
import org.xtext.example.mydsl.myDsl.Node
import org.xtext.example.mydsl.myDsl.Root
import org.xtext.example.mydsl.myDsl.UniNode
import org.xtext.example.mydsl.myDsl.Formula
import java.util.ArrayList

class RandomModelGenerator {
	
	Random randGen = new Random();
	val fact = MyDslFactoryImpl.init()
	
	//return a possible new son for a node with name name
	def generateRandomNewSon(String name, String fatherType) {
		if (fatherType == "OrGroup") {
			val n = fact.createUniNode()
			n.setName(name)
			n.setType("Or")
			return n
		}
		if (fatherType == "XorGroup") {
			val n = fact.createUniNode()
			n.setName(name)
			n.setType("Xor")
			return n
		}		
		val type = randGen.nextInt(4)
		if (type <= 1) {
			var n = fact.createUniNode()
			n.setName(name)
			switch (type) {
				case 0: 
					n.setType("Mandatory") 
				default:
					n.setType("Optional")
			}
			return n	
		}
		else {
			var n = fact.createMultiNode()
				switch (type) {
					case 2: 
						n.setType("OrGroup") 
					default:
						n.setType("XorGroup")
			}
			return n
		}
	}
	
	//add nb sons to a node n
	def addSons(Node n, int nb, String subname) {
		val sons = n.getSons()
		for (var i = 0; i < nb; i++) {
			sons.add(generateRandomNewSon('f'+subname + Integer.toString(i), n.getType()))
		}
	}
	
	def void setSons(Node n, int maxDepth, int range) {
		if (maxDepth != 0) {
		val nbSons = randGen.nextInt(range) + 1
		addSons(n, nbSons, Integer.toString(maxDepth))
		val sons = n.getSons()
		for (Node son : sons) {
			if (son.getType() == "XorGroup" || son.getType() == "OrGroup") {
				setSons(son, maxDepth, range)
			}
			else {
				setSons(son, maxDepth - 1, range)
			}
			}
		}
	}

	def ArrayList<String> getSonsNames(Node n) {
		val ArrayList<String> names = newArrayList()
		val sons = n.getSons()
		for (Node node: sons) {
			if (node instanceof UniNode) {
				names.add(node.getName())
			}
			names.addAll(getSonsNames(node))
		}
		return names
	}
	
	def ArrayList<String> getAllSonsNames(Root r) {
		val ArrayList<String> names = newArrayList()
		val sons = r.getSons()
		for (Node node: sons) {
			if (node instanceof UniNode) {
				names.add(node.getName())
			}
			names.addAll(getSonsNames(node))
		}
		return names
	}
	
	//return a random feature (positive or negative) from a list of feature as a formula
	def getRandomFeature(String[] names) {
		val r = randGen.nextInt(names.length())
		val f = fact.createFormula()
		f.setFeature(names.get(r))
		val pos = randGen.nextInt(2)
		if (pos == 0) {
			val g = fact.createNeg()
			g.setF(f)
			return g
		}
		return f
	}
	
	//create the formula a \/ b with a, b
	def Or(Formula f1, Formula f2) {
		val formula = fact.createBinary()
		formula.setLf(f1)
		formula.setOp("or")
		formula.setRf(f2)
		return formula
	}
	
	//create the formula a \/ b with a, b
	def And(Formula f1, Formula f2) {
		val formula = fact.createBinary()
		formula.setLf(f1)
		formula.setOp("and")
		formula.setRf(f2)
		return formula
	}
	
	//return a new Clause with nbLiteral as the number of litteral in the clause
	def getRandomClause(String[] names, int nbLiteral) {
		var f = getRandomFeature(names)
		for (var i = 0; i < nbLiteral; i++) {
			f = Or(f, getRandomFeature(names))
		}
		return f
	}
	
	def newModel(int depth, int range, int nbliteral, int nbClause) {
		val root = fact.createRoot()
		root.setName("Root")
		val nbSons = randGen.nextInt(range - 1) + 1
		for (var i = 0; i < nbSons; i++) {
			root.getSons.add(generateRandomNewSon("f"+Integer.toString(depth) + Integer.toString(i), "Root"))
		}
		val sons = root.getSons()
		for (Node son : sons) {
			setSons(son, depth - 1, range)
		}
		
		val constraints = root.getConstraint()
		val names = getAllSonsNames(root)
		for (var i = 0; i < nbClause; i++) {
			constraints.add(getRandomClause(names, nbliteral - 1))
		}			
		return root
	}


    def static void main(String[] args) {
   		val Test = new RandomModelGenerator
   		val Write = new writeToFile
   	    val truc = Test.newModel(2, 3, 2, 1)
   		println(Write.prettyPrint(truc))
   		
   		
    }
}



