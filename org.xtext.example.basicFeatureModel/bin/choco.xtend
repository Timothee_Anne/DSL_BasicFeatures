import static org.chocosolver.solver.constraints.nary.cnf.LogOp.and
import static org.chocosolver.solver.constraints.nary.cnf.LogOp.or
import static org.chocosolver.solver.constraints.nary.cnf.LogOp.nor
import org.chocosolver.solver.Model
import org.chocosolver.solver.variables.BoolVar
import org.chocosolver.solver.constraints.nary.cnf.LogOp
import java.util.Hashtable


public class choco {
	static var table = new Hashtable<String, BoolVar>	
	static var model = new Model("model")
	static var tmp = model.boolVar("tmp")
	
	def static LogOp cnfToChocoCnf(CNF cnf) {
		var f = model.boolVar(false)
		var clauses = nor(f)
		for (clause c: cnf.getList()){
			clauses = and(clauses, clauseToLogOp(c))
		}
		return clauses
	}
	
	def static LogOp clauseToLogOp(clause cl) {
		var t = model.boolVar(true)
		var clause = nor(t)
		for (literal l: cl.get())  {
			if (!table.containsKey(l.get())){
				tmp = model.boolVar(l.get())
    			table.put(l.get(),tmp)
    		}
    		else {
    			tmp = table.get(l.get())
    		}
    		
			if (l.toBool()) {
				clause = or(clause, tmp)
			}
			else {
				clause = or(clause, nor(tmp))
			}			
		}
		return clause
	}

    def static void main(String[] args) {
        // 1. Create a Model
        val mGen = new RandomModelGenerator()
        val cnfGen = new CNF()
        val write = new writeToFile
        
       	/*val model1 = mGen.newModel(2,2,2,1)
        val cnf1 = cnfGen.generateCNF(model1)
        val chocoCnf1 = cnfToChocoCnf(cnf1) */  
        
        
        val test = write.rootExample()    
        val testcnf = cnfGen.generateCNF(test)
		val testchocoCnf = cnfToChocoCnf(testcnf)
		
		model.addClauses(testchocoCnf)
		println(testchocoCnf)
		println(model)
		for (BoolVar bv: table.values) {
			System.out.println(bv)
		}
		System.out.println("\n")
		val solver = model.getSolver()

		while (solver.solve()) {
			for (BoolVar bv: table.values) {
				System.out.println(bv)
			}
			System.out.println("\n")
		}
		



        // Solve the problem      
     	/*val a = model.boolVar("a")
     	println(model)
        val b = model.boolVar(false)
        println(model)
        model.addClauses(or(a,b))
        System.out.println(a)
        System.out.println(b)
        System.out.println("\n")
        var solver = model.getSolver()        
        while(solver.solve()){
        	System.out.println(a)
        	System.out.println(b)
        	System.out.println("\n")
        }
        System.out.println(solver.isSatisfied())*/
	}
}