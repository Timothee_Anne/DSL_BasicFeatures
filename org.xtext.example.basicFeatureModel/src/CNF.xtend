import java.util.ArrayList;
import java.util.List;
import org.xtext.example.mydsl.myDsl.Root
import org.xtext.example.mydsl.myDsl.Node
import org.xtext.example.mydsl.myDsl.UniNode
import org.xtext.example.mydsl.myDsl.Formula
import org.xtext.example.mydsl.myDsl.Neg
import org.xtext.example.mydsl.myDsl.Binary

class CNF {
	val List<clause> clauseList = new ArrayList<clause>();
	var name = ""
	
	new(String str){
		name = str
	}
	
	new(){
		
	}
	def String getName(){
		name
	}
	def add(clause c){
		clauseList.add(c)
	}
	
	def addList(List<clause> list){
		for (clause c : list) { 
			clauseList.add(c)
		}
	}
	
	def int getSize(){
		clauseList.length()
	}
	
	override toString(){
		var String str = name + ": "
		for (clause c : clauseList) { 
			str = str + c.toString() + " & "
		}
		str = str.substring(0,max(0,str.length-2)) 
	}
	
	def getList(){
		clauseList
	}
	def max(int i, int j) {
		if (i >= j) i else j
	}
	

def CNF generateCNF(Root root){
	val cnf = new CNF(root.name)
	cnf.add(new clause(new literal(root.name,true)))
	for (son: root.sons){
		cnf.addList(generateClause(son))
		if(son instanceof UniNode ){
			val c1 = new clause(new literal(root.name,true))
			c1.add(new literal(son.name,false))
			cnf.add(c1)
			
			val c2 = new clause(new literal(root.name,false))
			c2.add(new literal(son.name,true))
			
			switch son.type {
				case "Mandatory": cnf.add(c2)
				
			}
		}
		else {
			switch son.type {
				case "OrGroup": cnf.add(OrClause(root.name, son)) 
				case "XorGroup": cnf.addList(XorClause(root.name,son))
			}
		}
	}
	cnf.addList(generateConstraints(root.constraint))
	cnf
}

def List<clause> generateConstraints(List<Formula> l){
	var list = new ArrayList<clause>
	for (f: l){
		list.add(generateFormula(f))
	}
	list
}

def clause generateFormula(Formula f){
	var c = new clause()
	if (f instanceof Neg) {
		c.add(new literal(f.getF.getFeature,false))
	}
	else if (f instanceof Binary){
		val cl = generateFormula(f.getLf())
		val cr = generateFormula(f.getRf())
		c.addClause(cl)
		c.addClause(cr)
	}
	else {
		c.add(new literal(f.getFeature,true))
	}
	c
}

def clause OrClause(String GrandFatherName, Node node){
	val c = new clause(new literal(GrandFatherName, false))
	for (son: node.sons){
		if (son instanceof UniNode){
			c.add(new literal(son.name,true))
		}
	}
	c
}

def List<clause> XorClause(String GrandFatherName, Node node){
	var list = new ArrayList<clause>
	val c = new clause(new literal(GrandFatherName, false))
	for (son: node.sons){
		if (son instanceof UniNode){
			c.add(new literal(son.name,true))
			for (son2: node.sons){
				if (son2 instanceof UniNode){
					if (son2.name != son.name){
						var c2 = new clause(new literal(GrandFatherName, false))
						c2.add(new literal(son.name,false))
						c2.add(new literal(son2.name,false))
						list.add(c2)
					}
				}
			}
		}
	}
	list.add(c)
	list
}


def List<clause> generateClause(Node node){
	var list = new ArrayList<clause>
	if(node instanceof UniNode ){
		for (son: node.sons){
			list.addAll( generateClause(son))
			if(son instanceof UniNode ){
				println(node.type)
				///list.addAll(generateClause(son,node.name))
				val c1 = new clause(new literal(node.name,true))
				c1.add(new literal(son.name,false))
				list.add(c1)
				
				val c2 = new clause(new literal(node.name,false))
				c2.add(new literal(son.name,true))
				
				switch son.type {
					case "Mandatory": list.add(c2)
					
				}
			}
			else {
				switch son.type {
					case "OrGroup": list.add(OrClause(node.name, son)) 
					case "XorGroup": list.addAll(XorClause(node.name,son))
				}
			}
		}
	}
	else {
		for (son: node.sons){
			list.addAll( generateClause(son))
		}
	}
	list
}

}


