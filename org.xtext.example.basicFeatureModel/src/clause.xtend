import java.util.ArrayList;
import java.util.List;

class clause {
	val List<literal> clause = new ArrayList<literal>();
	
	
	new(){
		
	}
	new(literal l){
		this.add(l)
	}
	
	def get(){
		clause
	}
	def add(literal l){
		clause.add(l)
	}
	
	def addClause(clause c){
		clause.addAll(c.get())
	}
	
	override toString(){
		var String str = "("
		for (literal l: clause) { 
			str = str + l.toString() + "|"
		}
		str = str.substring(0,max(0,str.length-1))  + ")"

	
	}
	def max(int i, int j) {
		if (i >= j) i else j
	}
}