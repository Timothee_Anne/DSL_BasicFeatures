import org.eclipse.swt.widgets.Display
import org.eclipse.swt.widgets.Shell
import org.eclipse.jface.wizard.WizardDialog

class testWizard {
	def static void main(String[] args) {
	var display = Display.getCurrent()
	
	if ( display === null )
    {
      display = new Display();
    }
    var wizard = new myWizard()
	var Shell shell = new Shell( display );
	var WizardDialog dialog = new WizardDialog(shell, wizard);
	dialog.open();

}

}