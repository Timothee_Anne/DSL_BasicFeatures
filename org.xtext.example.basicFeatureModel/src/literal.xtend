
class literal {
	var Boolean bool = true
	var String name = ""
	
	new(String str, Boolean b) {
		bool = b
		name = str
	}
	
	def get(){
		name
	}
	
	def toBool(){
		bool
	}
	override toString() {
		if (bool) {
			name
		}
		else {
			"~"+name
		}
				
	}
}